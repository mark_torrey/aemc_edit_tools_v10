﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace AEMC.EditWorkflow_10
{
    public class ServiceOrderComboBox : ESRI.ArcGIS.Desktop.AddIns.ComboBox
    {

        EditWorkflowController _ext = null;

        /*
         * constructor
         */
        public ServiceOrderComboBox()
        {
            _ext = EditWorkflowController.GetExtension();
            
        }

        /*
         * update event handler
         */
        protected override void OnUpdate()
        {
            this.Enabled = _ext.isEnabled && !_ext.isEditing;
        }

        internal string GetText()
        {
            return this.Value;
        }

        internal void clear()
        {
            this.Clear();
        }

    }

}
