﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ESRI.ArcGIS.Desktop.AddIns;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Framework;


namespace AEMC.EditWorkflow_10
{
    public class SyncLocalDatabaseButton : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        #region FIELDS

        private EditWorkflowController _ext = null;
        
        #endregion

        #region PROPERTIES
        #endregion
       
        #region METHODS

        /**
         * constructor
         **/
        public SyncLocalDatabaseButton()
        {
            _ext = EditWorkflowController.GetExtension();
        }
        
        /**
         * 
         **/
        protected override void OnClick()
        {
            _ext.SynchronizeReplica();
        }

        /**
         * 
         **/
        protected override void OnUpdate()
        {
           Enabled = !_ext.isEditing && _ext.AppConfig.LocalDatabase;

        
            
        }

        public void setToolTip(string newValue)
        {
            
        }
        #endregion
    }
}
