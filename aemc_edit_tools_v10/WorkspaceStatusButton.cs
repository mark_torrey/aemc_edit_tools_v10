﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ESRI.ArcGIS.Framework;
using ESRI.ArcGIS.esriSystem;

namespace AEMC.EditWorkflow_10
{
    public class WorkspaceStatusButton : ESRI.ArcGIS.Desktop.AddIns.Button
    {
        #region FIELDS

        private EditWorkflowController _ext = null;

        #endregion

        #region PROPERTIES

        ICommandItem cmdItem
        {
            get
            {
                UID btnGUID = new UIDClass();
                btnGUID.Value = ThisAddIn.IDs.WorkspaceStatusButton;

                ICommandItem ci = ArcMap.Application.Document.CommandBars.Find(btnGUID, true, true);
                if (ci != null)
                {
                    return ci;
                }
                else
                {
                    return null;
                }
            }
        }
        #endregion

        #region METHODS

        /**
         * constructor
         **/
        public WorkspaceStatusButton()
        {
            _ext = EditWorkflowController.GetExtension();

            
            
            

        }

        public void SetCaption(string ToValue)
        {
            string cpt = "Last Updated: Unknown";
            if (cmdItem != null)
                cpt = string.Format("{0}: {1}", "Last Updated", ToValue);
                cmdItem.Caption = cpt;
        }

        /**
         * button click event handler 
         **/
        protected override void OnClick()
        {
            
        }

        /**
         * system update event handler
         **/
        protected override void OnUpdate()
        {
            Enabled = false;
        }

        #endregion

    }
}
