﻿using System;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Collections;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.ArcMapUI;
using ESRI.ArcGIS.Framework;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.Geoprocessor;
using ESRI.ArcGIS.Desktop.AddIns;
using ESRI.ArcGIS.DataManagementTools;
using System.ComponentModel;
using ESRI.ArcGIS.DataSourcesGDB;
using AEMC.EditWorkflow_10;
using System.Threading;

namespace AEMC.EditWorkflow_10
{
    public class EditWorkflowController : ESRI.ArcGIS.Desktop.AddIns.Extension
    {
        #region FIELDS
        
        private static string FEATURELAYERID = "{6CA416B1-E160-11D2-9F4E-00C04F6BC78E}";

        private static EditWorkflowController _extension;

        private string _editVersionName = null;
        private string _currentServiceOrder = null;
        private bool _layersinmap = false;
        private bool _islocalworkspace = false;
        private Settings _appconfig = null;
        private IMouseCursor _mc = new MouseCursorClass();
        private bool _singleWorkspace = false;
        private ArrayList _editLayers = null;
        IProgressDialog2 _progressDialog = null;
        string _localmxdPath = string.Empty;

        #endregion

        #region PROPERTIES

        internal string getLocalGDBName
        {
            get{
                string hostName = System.Environment.MachineName.Replace("-", "_");
                string workingFolder = this.AppConfig.LocalWorkingDir;
                if (!System.IO.Directory.Exists(workingFolder))
                {
                    MessageBox.Show("Local Workspace Not Specified", "Local Workspace", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return null;
                }

               return Path.Combine(workingFolder, string.Format("{0}.{1}", hostName, "gdb"));
            }
        }

        /**
         * Check if we are currently in an edit session
         **/
        internal bool isEditing
        {
            get
            {
                return ArcMap.Editor.EditState == ESRI.ArcGIS.Editor.esriEditState.esriStateEditing;
            }
        }

        /**
         * Name of the current edit version
         **/
        internal string CurrentEditVersionName
        {
            get
            {
                return _editVersionName;
            }
        }

        /**
         * The current value of the service order
         **/
        internal string CurrentServiceOrderValue
        {
            get
            {
                if (_currentServiceOrder == null)
                    _currentServiceOrder = "Unknown";

                return _currentServiceOrder;
            }
            set
            {
                _currentServiceOrder = value;
            }
        }

        /**
         * Is Extension Enabled
         **/
        internal bool isEnabled
        {
            get
            {
                return _layersinmap;
            }
        }

        /**
         * Application Configuration
         **/
        internal Settings AppConfig {
            get
            {
                return _appconfig;
            }
        }
        
        #endregion

        #region METHODS

        /**
         * return an instance of yourself
         **/
        internal static EditWorkflowController GetExtension()
        {
            if (_extension == null)
            {
                UID extID = new UIDClass();
                extID.Value = ThisAddIn.IDs.EditWorkflowController;
                ArcMap.Application.FindExtensionByCLSID(extID);
            }
            return _extension;
        }

        /**
         * constructor
         **/
        public EditWorkflowController()
        {
            _extension = this;

            AddIn.FromID<WorkspaceStatusButton>(ThisAddIn.IDs.WorkspaceStatusButton);

        }

        /**
         * 
         **/
        internal void ValidateLocalReplica()
        {
        }

        /**
         * 
         **/
        internal void CreateNewReplica()
        {
            string hostName = System.Environment.MachineName.Replace("-", "_");
            string workingFolder = this.AppConfig.LocalWorkingDir;
            if (!System.IO.Directory.Exists(workingFolder)){
                MessageBox.Show("Local Workspace Not Specified", "Local Workspace", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            string localgdb = Path.Combine(workingFolder, string.Format("{0}.{1}",hostName, "gdb"));
            if (!Directory.Exists(localgdb))
            {
                DialogResult dr = MessageBox.Show("A local geodatabase was not found in the working folder, Would you like to create one now?", "Local Workspace", MessageBoxButtons.YesNo);
                if (dr == DialogResult.Yes)
                {
                    IVariantArray p = new VarArrayClass();
                    p.Add(_appconfig.LocalWorkingDir);
                    runGPModel("createreplica_aemc_tools", "Creating Local Geodatabase...",p);
                }
                else
                {
                    return;
                }
            }
            else
            {
                IWorkspaceFactory2 localws = new FileGDBWorkspaceFactoryClass();
                IWorkspace w = localws.OpenFromFile(localgdb, 0);

                if (CheckInputControlValues() == false)
                {
                    return;
                }

                DesignIdComboBox dic = AddIn.FromID<DesignIdComboBox>(ThisAddIn.IDs.DesignIdComboBox);
                this._editVersionName = dic.GetText();

                ArcMap.Editor.StartEditing(w);

                Marshal.ReleaseComObject(w);
                w = null;
                
            }
        }

        /**
         * 
         **/
        private void runGPModel(string withName, string withMessage, IVariantArray withParams)
        {
            try
            {
                ITrackCancel tc = new ESRI.ArcGIS.Display.CancelTrackerClass();
                IProgressDialogFactory pd = new ProgressDialogFactoryClass();
                IStepProgressor sp = pd.Create(tc, ArcMap.Application.hWnd);
                _progressDialog = (IProgressDialog2)sp;

                sp.Hide();
                _progressDialog.Animation = esriProgressAnimationTypes.esriProgressSpiral;
                _progressDialog.CancelEnabled = false;
                _progressDialog.Description = withMessage ;
                _progressDialog.ShowDialog();

                Geoprocessor gp = new Geoprocessor();
                gp.OverwriteOutput = true;
               
                //gp.ToolExecuted += new EventHandler<ToolExecutedEventArgs>(GpToolDidExecute);
                //todo: pull this from the assembly
                string toolboxPath = Path.Combine(_appconfig.LocalWorkingDir, "AEMC_TOOLS.tbx");
                gp.AddToolbox(toolboxPath);
                
                gp.Execute(withName, withParams, null);

                if (gp.MessageCount > 0)
                {
                    for (int msgNdx = 0; msgNdx <= gp.MessageCount - 1; msgNdx++)
                    {
                        Debug.WriteLine(gp.GetMessage(msgNdx));
                    }
                }

                _progressDialog.HideDialog();

                ArcMap.Application.OpenDocument(_localmxdPath);
                
            }
            catch (Exception exc)
            {
                Debug.WriteLine(exc.Message);
                MessageBox.Show("Unable to create local db");
                _progressDialog.HideDialog();
            }
        }

        /**
         * 
         **/
        void GpToolDidExecute(object sender, ToolExecutedEventArgs e)
        {
            

            /*IVariantArray p = new VarArrayClass();
            *p.Add(_appconfig.LocalWorkingDir);
            *runGPModel("createreplica_aemc_tools", "Creating Local Geodatabase...", p);
            */   
        }

        /**
         *
         **/
        internal void SynchronizeReplica()
        {
            ITemplates tmlates = ArcMap.Application.Templates;
            _localmxdPath = ((IDocumentInfo2)ArcMap.Document).Path;
            ArcMap.Application.NewDocument(false, tmlates.get_Item(0));
            Application.DoEvents();

            IVariantArray va = new VarArrayClass();
            string lgn = getLocalGDBName;
            va.Add(lgn);

            runGPModel("syncthencreatelocalreplica_aemc_tools", "Synchronizing Local Geodatabase...", va);
        }

        /**
         * 
         **/
        internal bool CheckInputControlValues()
        {
            DesignIdComboBox dic = AddIn.FromID<DesignIdComboBox>(ThisAddIn.IDs.DesignIdComboBox);
            if (dic.GetText() == null || dic.GetText().Length == 0)
            {
                MessageBox.Show("Please Enter a work order number to begin editing", "Work Order");
                return false;
            }
                
            this.CurrentServiceOrderValue = AddIn.FromID<ServiceOrderComboBox>(ThisAddIn.IDs.ServiceOrderComboBox).GetText();
            if (this.CurrentServiceOrderValue == null || this.CurrentServiceOrderValue.Length == 0)
            {
                MessageBox.Show("Please Enter a service order number to begin editing", "Service Order");
                return false;
            }

            return true;
        }

        /**
         * Create a new edit version
         **/
        internal void CreateNewVersion()
        {
            _mc.SetCursor(2);
            _editLayers = this.getMapLayers();

            if (_singleWorkspace && _editLayers.Count > 0)
            {
                string versionName = string.Empty;
                IWorkspace parentWorkspace = ((IDataset)((IFeatureLayer)_editLayers[0]).FeatureClass).Workspace;
                IVersion parentVersion = (IVersion)parentWorkspace;

                if (CheckInputControlValues() == false)
                {
                    return;
                }

                DesignIdComboBox dic = AddIn.FromID<DesignIdComboBox>(ThisAddIn.IDs.DesignIdComboBox);
                _editVersionName = dic.GetText();
                
                IVersion editversion;
                try
                {
                    editversion = parentVersion.CreateVersion(_editVersionName);
                    editversion.Access = esriVersionAccess.esriVersionAccessPublic;
                }
                catch (COMException cesx)
                {
                    MessageBox.Show(cesx.Message);
                    
                    return;
                }


                ((IMapAdmin)ArcMap.Document.FocusMap).FireChangeVersion(parentVersion, editversion);
                IChangeDatabaseVersion cdv = new ChangeDatabaseVersion();
                cdv.Execute(parentVersion, editversion, (IBasicMap)ArcMap.Document.FocusMap);
              
                ArcMap.Editor.StartEditing((IWorkspace)editversion);

            }
            else
            {
                Debug.WriteLine("Multiple Workspaces in mxd");
            }

            _mc.SetCursor(0);
        }

        /**
         * Return list of layers to workagainst
         **/
        private ArrayList getMapLayers()
        {
            IMap m = ArcMap.Document.FocusMap;
            UID lyrID = new UIDClass();
            lyrID.Value = FEATURELAYERID;

            IEnumLayer lyrs = m.get_Layers(lyrID, true);
            lyrs.Reset();

            ILayer pLayer = lyrs.Next();
            IWorkspace currentWrkSp = null;
            ArrayList changeLayers = new ArrayList();
            HashSet<IWorkspace> w = new HashSet<IWorkspace>();

            while (pLayer != null)
            {
                if (pLayer is IFeatureLayer)
                {
                    currentWrkSp = ((IDataset)((IFeatureLayer)pLayer).FeatureClass).Workspace;
                    if (currentWrkSp is IVersion)
                    {
                        changeLayers.Add(pLayer);
                        w.Add(currentWrkSp);
                    }
                }
                pLayer = lyrs.Next();
            }

            _singleWorkspace = w.Count == 1;
            return changeLayers;
        }

        /**
         * Reconcile, post then delete the current edit workspace
         **/
        internal void ReconcilePostVersion()
        {
            
            _mc.SetCursor(2);

            IVersionEdit4 ve4 = ((IVersionEdit4)ArcMap.Editor.EditWorkspace);
            
            IVersionedWorkspace vw = (IVersionedWorkspace)ve4;
            IVersionInfo vp = vw.DefaultVersion.VersionInfo;
            
            IVersion editV = (IVersion)ve4;
            string editVName = editV.VersionName;
            
            string parentVName = vp.VersionName;
            IVersion prntV = vw.FindVersion(parentVName);

            DialogResult dr = MessageBox.Show("Do you want to save your edits?", "Save", MessageBoxButtons.YesNoCancel);
            if (dr != DialogResult.Yes)
            {
                stopEditSession(false, editV, prntV);
                return;
            }
            
            bool hasConflicts = ve4.Reconcile4(parentVName, true, false, true, true);
           
            try
            {
                if (hasConflicts)
                {
                    MessageBox.Show("Conflicts Detected");
                    UID cmdId = new UIDClass();
                    cmdId.Value = "{D90B1761-95FF-11D2-8526-0000F875B9C6}";

                    ICommandItem conflictCommand = ArcMap.Application.Document.CommandBars.Find(cmdId, true, true);
                    if (conflictCommand != null)
                    {
                        conflictCommand.Execute();
                    }
                }
                else
                {
                    ve4.Post(parentVName);

                    stopEditSession(true, editV, prntV);
                    
                }

            }
            catch
            {
                MessageBox.Show("Unable to Reconcile", "Reconcile",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            finally
            {
                _mc.SetCursor(0);
            
            }  
        }

        /**
         * stop local fgdb editing
         **/
        internal void stopLocalGDBEditSession()
        {
            DialogResult dr = MessageBox.Show("Do you want to save your edits?", "Save", MessageBoxButtons.YesNoCancel);
            if (dr != DialogResult.Yes)
            {
                ArcMap.Editor.StopEditing(false);
                return;
            }
            else
            {
                ArcMap.Editor.StopEditing(true);
            }

            AddIn.FromID<DesignIdComboBox>(ThisAddIn.IDs.DesignIdComboBox).clear();
            AddIn.FromID<ServiceOrderComboBox>(ThisAddIn.IDs.ServiceOrderComboBox).clear();
        }

        /**
         * 
         **/
        internal void stopEditSession(bool saveEdits, IVersion editVersion, IVersion parentVersion)
        {
            ArcMap.Editor.StopEditing(saveEdits);

            AddIn.FromID<DesignIdComboBox>(ThisAddIn.IDs.DesignIdComboBox).clear();
            AddIn.FromID<ServiceOrderComboBox>(ThisAddIn.IDs.ServiceOrderComboBox).clear();

            IChangeDatabaseVersion cdv = new ChangeDatabaseVersion();
            ISet d = cdv.Execute(editVersion, parentVersion, (IBasicMap)ArcMap.Document.FocusMap);
            editVersion.RefreshVersion();
            parentVersion.RefreshVersion();

            ((IMapAdmin)ArcMap.Document.FocusMap).FireChangeVersion(editVersion, parentVersion);

            string wname = ((IWorkspace)parentVersion).PathName;
            string oldname = editVersion.VersionName;

            DeleteEditVersion(wname, oldname);
        }

        /**
         * 
         **/
        internal void DeleteEditVersion(string fromWorkspacepath, string withName)
        {
            try
            {
                Thread t = new Thread(bw_DoWork);
                t.SetApartmentState(ApartmentState.STA);
               
                //BackgroundWorker bw = new BackgroundWorker();
                
                //bw.DoWork += bw_DoWork;
                //bw.RunWorkerCompleted += bw_RunWorkerCompleted;

                ArrayList al = new ArrayList();
                al.Add(fromWorkspacepath);
                al.Add(withName);
                t.Start(al);
                //Tuple<string, string> t = new Tuple<string, string>(fromWorkspacepath, withName);
             
               // bw.RunWorkerAsync(al);
                
            }
            catch (COMException cmexc)
            {
                Debug.WriteLine(cmexc.ErrorCode);
            }
        }

        /**
         * 
         **/
        internal void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Debug.WriteLine("Version deleted");
        }

        /**
         * 
         **/
        internal void bw_DoWork(object p)/*object sender, DoWorkEventArgs e)*/
        {
            ArrayList t = (ArrayList)p;
            string wrksp = t[0].ToString();
            string name = t[1].ToString();

            Type tt = Type.GetTypeFromProgID("esriDataSourcesGDB.SdeWorkspaceFactory");
            IWorkspaceFactory pFactory = (IWorkspaceFactory)Activator.CreateInstance(tt);
            IWorkspace w = pFactory.OpenFromFile(wrksp, 0);//IWorkspace pWorkspace = pFactory.Open(pPropertySet, 0);
            
            IVersionedWorkspace vw = (IVersionedWorkspace)w;
            IVersion v = vw.FindVersion(name);
            v.Delete();
            
        }

        /**
         * 
         **/
        internal void DeleteVersionDidComplete(object sender, ToolExecutedEventArgs e)
        {
            Debug.WriteLine("GP Tool Did Execute");
        }
        
        /**
         * Extension startup event handler
         **/
        protected override void OnStartup()
        {
            _appconfig = new SettingsController().AppSettings;

            WireDocumentEvents();
        }

        /**
         * 
         **/
        private void CheckForConfigLayers()
        {
            
            IMaps maps = ((IMxDocument)ArcMap.Document).Maps;
            IMap m = null;
            UID flyrID = null;
            IEnumLayer lyrs = null;
            ILayer pLayer = null;

            try
            {
                for (int map = 0; map <= maps.Count - 1; map++)
                {
                    m = maps.get_Item(map);
                    flyrID = new UIDClass();
                    flyrID.Value = FEATURELAYERID;
                    lyrs = m.get_Layers(flyrID, true);
                    lyrs.Reset();
                    pLayer = lyrs.Next();
                    while (pLayer != null)
                    {
                        if (pLayer.Valid)
                        {
                            string dsName = ((IDataset)((IFeatureLayer)pLayer).FeatureClass).Name.ToUpper();
                            if (AppConfig.getLayerNames().Contains(dsName))
                            {
                                this._layersinmap = true;
                                IWorkspace w = ((IDataset)((IFeatureLayer)pLayer).FeatureClass).Workspace;

                                GetReplicaInfo(w);
                                return;
                            }
                        }
                        
                        pLayer = lyrs.Next();
                    }
                }

                this._layersinmap = false;
            } catch(Exception x){
                Debug.WriteLine(x.Message);
            } finally{
                maps = null;
                m = null;
                flyrID = null;
                lyrs = null;
                pLayer = null;
            }
        }

        /**
         * retrieve replica info from workspace and update ui with value
         **/
        private void GetReplicaInfo(IWorkspace fromWorkspace)
        {
            IWorkspaceReplicas2 wr = (IWorkspaceReplicas2)fromWorkspace;
            
            string hostName = System.Environment.MachineName.Replace("-", "_");
            IReplica2 r = (IReplica2)wr.get_ReplicaByName(hostName);
            WorkspaceStatusButton wsb = AddIn.FromID<WorkspaceStatusButton>(ThisAddIn.IDs.WorkspaceStatusButton);
            if (r != null)
            {
                DateTime dt = DateTime.FromOADate(r.ReplicaDate);
                string rv = dt.ToString("F");

                wsb.SetCaption(rv);
            }
        }

        /**
         * 
         **/
        private void CheckMXD()
        {
            string mxdPath = ((IDocumentInfo2)ArcMap.Document).Path;
            bool localmxd = Path.GetFileNameWithoutExtension(mxdPath).EndsWith("_local");

            this.AppConfig.LocalDatabase = localmxd;

            SyncLocalDatabaseButton sdb = AddIn.FromID<SyncLocalDatabaseButton>(ThisAddIn.IDs.SyncLocalDatabaseButton);
            
        }

        /**
         * Start listening to ArcMAP Document events
         **/
        private void WireDocumentEvents()
        {
            
            ArcMap.Events.MapsChanged += delegate()
            {
                CheckForConfigLayers();
            };

            ArcMap.Events.OpenDocument += delegate() {
                CheckMXD();
                CheckForConfigLayers();
            };

            ArcMap.Events.NewDocument += delegate() {
                CheckForConfigLayers(); 
            };

            ArcMap.Events.ActiveViewChanged += delegate()
            {
                CheckForConfigLayers();
            };
        }

        #endregion
    }
}
